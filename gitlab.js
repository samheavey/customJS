function validate(e){
  if (!e.target.value.includes('### Problem:\n') || !e.target.value.includes('### Solution:\n')){
    alert("Description format incorrect")
  }
}

issue_text_box = document.getElementById("issue_description")

if (issue_text_box && window.location.pathname.includes("issues/new")) {
  if (!issue_text_box.value){
    issue_text_box.value = "### Problem:\n\n\n### Solution:\n"  
  }
  issue_text_box.addEventListener("change", validate);
}
